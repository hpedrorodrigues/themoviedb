package com.hpedrorodrigues.movieapp.ui.base;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class BaseAdapter<M, V extends BaseHolder> extends RecyclerView.Adapter<V> {

    private List<M> content;

    @Override
    public int getItemCount() {
        return getContent().size();
    }

    public List<M> getContent() {
        return content == null ? Collections.<M>emptyList() : content;
    }

    public void setContent(List<M> content) {
        this.content = content;
        notifyDataSetChanged();
    }

    public void addContent(List<M> content) {
        final List<M> list = new ArrayList<>();

        list.addAll(getContent());
        list.addAll(content);

        setContent(list);
    }

    public void clear() {
        setContent(null);
    }
}
