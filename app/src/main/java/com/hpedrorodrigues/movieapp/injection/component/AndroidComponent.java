package com.hpedrorodrigues.movieapp.injection.component;

import com.hpedrorodrigues.movieapp.injection.module.AndroidModule;
import com.hpedrorodrigues.movieapp.ui.composer.detail.MovieDetailActivity;
import com.hpedrorodrigues.movieapp.ui.composer.list.MovieListActivity;
import com.hpedrorodrigues.movieapp.ui.composer.list.MovieListPresenter;
import com.hpedrorodrigues.movieapp.ui.composer.search.MovieSearchActivity;
import com.hpedrorodrigues.movieapp.ui.composer.search.MovieSearchPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AndroidModule.class})
public interface AndroidComponent {

    void inject(final MovieListPresenter presenter);

    void inject(final MovieListActivity activity);

    void inject(final MovieSearchPresenter presenter);

    void inject(final MovieSearchActivity activity);

    void inject(final MovieDetailActivity activity);
}