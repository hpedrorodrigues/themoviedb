package com.hpedrorodrigues.movieapp.data.service.interceptor;

import com.hpedrorodrigues.movieapp.data.contract.ApiContract;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthorizationInterceptor implements Interceptor {

    @Override
    public Response intercept(final Chain chain) throws IOException {
        final Request original = chain.request();
        final HttpUrl originalUrl = original.url();

        final HttpUrl url = originalUrl
                .newBuilder()
                .addQueryParameter("api_key", ApiContract.API_KEY)
                .build();

        final Request request = original.newBuilder()
                .url(url)
                .build();

        return chain.proceed(request);
    }
}