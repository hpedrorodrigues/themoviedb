package com.hpedrorodrigues.movieapp.ui.composer.list;

import com.hpedrorodrigues.movieapp.R;
import com.hpedrorodrigues.movieapp.data.remote.ApiPage;
import com.hpedrorodrigues.movieapp.data.service.TMDService;

import javax.inject.Inject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

public class MovieListPresenter implements MovieListContract.Presenter {

    private final MovieListContract.View view;
    private final CompositeSubscription subscriptions;

    @Inject
    protected TMDService service;

    private int page;

    public MovieListPresenter(final MovieListContract.View view) {
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.page = 1;
    }

    @Override
    public void start() {
        loadData(false);
    }

    @Override
    public void stop() {
        this.subscriptions.unsubscribe();
    }

    @Override
    public void loadData(final boolean appendContent) {
        final Subscription subscription = service
                .getPopularMovies(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ApiPage>() {
                    @Override
                    public void call(final ApiPage page) {
                        if (appendContent) {
                            view.add(page);
                        } else {
                            view.refresh(page);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Timber.e(throwable, "Error getting movies page");
                        view.showError(R.string.occurred_error);
                    }
                });

        this.subscriptions.add(subscription);
    }

    @Override
    public void resetPage() {
        this.page = 1;
    }

    @Override
    public void increasePage() {
        this.page++;
    }
}