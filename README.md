# The Movie DB

A simple android app that uses [The Movie DB API][the-movie-db-api] and MVP.

## Images

See some images [here][images].

[the-movie-db-api]: https://www.themoviedb.org/documentation/api
[images]: ./IMAGES.md