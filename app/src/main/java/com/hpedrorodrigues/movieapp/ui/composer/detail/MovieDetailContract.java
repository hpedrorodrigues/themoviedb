package com.hpedrorodrigues.movieapp.ui.composer.detail;

import com.hpedrorodrigues.movieapp.ui.base.BasePresenter;
import com.hpedrorodrigues.movieapp.ui.base.BaseView;

public interface MovieDetailContract {

    interface View extends BaseView {

    }

    interface Presenter extends BasePresenter {

    }
}