package com.hpedrorodrigues.movieapp.ui.composer.list;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;

import com.hpedrorodrigues.movieapp.R;
import com.hpedrorodrigues.movieapp.data.remote.ApiPage;
import com.hpedrorodrigues.movieapp.ui.base.movie.BaseMovieListActivity;
import com.hpedrorodrigues.movieapp.ui.composer.list.adapter.MovieAdapter;
import com.hpedrorodrigues.movieapp.ui.composer.search.MovieSearchActivity;
import com.malinskiy.superrecyclerview.OnMoreListener;

import javax.inject.Inject;

import static android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;

public class MovieListActivity extends BaseMovieListActivity implements MovieListContract.View {

    @Inject
    public MovieAdapter adapter;

    private MovieListPresenter presenter;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        if (this.presenter == null) {
            this.presenter = new MovieListPresenter(this);
            getComponent().inject(presenter);
        }

        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            startActivity(new Intent(this, MovieSearchActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.movie_list;
    }

    @Override
    protected void injectMembers() {
        getComponent().inject(this);
    }

    @Override
    protected OnRefreshListener getRefreshListener() {
        return new OnRefreshListener() {

            @Override
            public void onRefresh() {
                presenter.resetPage();
                presenter.loadData(false);
            }
        };
    }

    @Override
    protected OnMoreListener getMoreListener() {
        return new OnMoreListener() {

            @Override
            public void onMoreAsked(final int overallItemsCount, final int itemsBeforeMore,
                                    final int maxLastVisiblePosition) {
                presenter.increasePage();
                presenter.loadData(true);
            }
        };
    }

    @Override
    protected MovieAdapter getAdapter() {
        return adapter;
    }

    @Override
    protected MovieListPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void refresh(final ApiPage page) {
        adapter.setContent(page.getResults());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void add(final ApiPage page) {
        adapter.addContent(page.getResults());
    }
}