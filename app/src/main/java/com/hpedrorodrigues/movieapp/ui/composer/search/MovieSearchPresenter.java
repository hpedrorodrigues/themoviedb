package com.hpedrorodrigues.movieapp.ui.composer.search;

import com.hpedrorodrigues.movieapp.R;
import com.hpedrorodrigues.movieapp.data.remote.ApiPage;
import com.hpedrorodrigues.movieapp.data.service.TMDService;

import javax.inject.Inject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class MovieSearchPresenter implements MovieSearchContract.Presenter {

    private final MovieSearchContract.View view;

    @Inject
    protected TMDService service;

    private Subscription subscription;

    private int page;

    public MovieSearchPresenter(final MovieSearchContract.View view) {
        this.view = view;
        this.page = 1;
    }

    @Override
    public void start() {
    }

    @Override
    public void stop() {
        cancelSubscription();
    }

    @Override
    public void loadData(final String query, final boolean appendContent) {
        cancelSubscription();

        subscription = service
                .searchPopularMovies(page, query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ApiPage>() {

                    @Override
                    public void call(final ApiPage page) {

                        if (appendContent) {
                            view.add(page);
                        } else {
                            view.refresh(page);
                        }
                    }
                }, new Action1<Throwable>() {

                    @Override
                    public void call(Throwable throwable) {
                        Timber.e(throwable, "Error searching movies page");
                        view.showError(R.string.occurred_error);
                    }
                });
    }

    @Override
    public void resetPage() {
        this.page = 1;
    }

    @Override
    public void increasePage() {
        this.page++;
    }

    private void cancelSubscription() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}