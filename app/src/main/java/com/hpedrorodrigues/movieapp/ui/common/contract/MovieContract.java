package com.hpedrorodrigues.movieapp.ui.common.contract;

import com.hpedrorodrigues.movieapp.data.remote.ApiPage;
import com.hpedrorodrigues.movieapp.ui.base.BasePresenter;
import com.hpedrorodrigues.movieapp.ui.base.BaseView;

public interface MovieContract {

    interface View extends BaseView {

        void refresh(final ApiPage page);

        void add(final ApiPage page);

        void showError(final int resourceId);
    }

    interface Presenter extends BasePresenter {

        void resetPage();

        void increasePage();
    }
}