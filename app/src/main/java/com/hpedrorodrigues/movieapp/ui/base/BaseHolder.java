package com.hpedrorodrigues.movieapp.ui.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class BaseHolder extends RecyclerView.ViewHolder {

    public BaseHolder(View view) {
        super(view);

        onView(view);
    }

    protected abstract void onView(final View view);
}