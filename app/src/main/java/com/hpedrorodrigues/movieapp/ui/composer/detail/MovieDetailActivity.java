package com.hpedrorodrigues.movieapp.ui.composer.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.hpedrorodrigues.movieapp.R;
import com.hpedrorodrigues.movieapp.data.constant.Args;
import com.hpedrorodrigues.movieapp.data.format.TMDDataFormatter;
import com.hpedrorodrigues.movieapp.data.remote.ApiResult;
import com.hpedrorodrigues.movieapp.ui.base.BaseActivity;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

public class MovieDetailActivity extends BaseActivity implements MovieDetailContract.View {

    private ImageView image;
    private TextView title;
    private TextView releaseDate;
    private TextView overview;

    private MovieDetailPresenter presenter;

    @Inject
    protected TMDDataFormatter formatter;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_detail);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        if (presenter == null) {
            presenter = new MovieDetailPresenter(this);
        }

        presenter.start();
    }

    @Override
    protected void onConfigureActionBar(final ActionBar actionBar) {
        super.onConfigureActionBar(actionBar);

        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onView() {
        image = (ImageView) findViewById(R.id.image);
        title = (TextView) findViewById(R.id.title);
        releaseDate = (TextView) findViewById(R.id.release_date);
        overview = (TextView) findViewById(R.id.overview);
    }

    @Override
    protected void onConfigView() {
        final ApiResult result = (ApiResult) getIntent().getExtras().getSerializable(Args.MOVIE);

        title.setText(result.getTitle());
        releaseDate.setText(formatter.formatDate(result.getReleaseDate()));
        overview.setText(result.getOverview());

        Picasso.with(this).load(formatter.getNormalImage(result.getPosterPath()))
                .placeholder(R.mipmap.movie).into(image);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.stop();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void injectMembers() {
        getComponent().inject(this);
    }
}