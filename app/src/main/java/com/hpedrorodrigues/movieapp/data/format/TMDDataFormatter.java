package com.hpedrorodrigues.movieapp.data.format;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import timber.log.Timber;

public class TMDDataFormatter {

    private final DateFormat inputFormat;
    private final DateFormat outputFormat;

    @Inject
    public TMDDataFormatter() {
        this.inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        this.outputFormat = new SimpleDateFormat("yyyy", Locale.getDefault());
    }

    private String getImage(final String size, final String path) {
        if (path == null || path.isEmpty()) {
            return null;
        }

        return "https://image.tmdb.org/t/p/" + size + path;
    }

    public String getThumbnailImage(final String path) {
        return getImage("w300", path);
    }

    public String getNormalImage(final String path) {
        return getImage("w780", path);
    }

    public String getLargeImage(final String path) {
        return getImage("w1280", path);
    }

    public String formatDate(final String text) {
        try {
            final Date date = inputFormat.parse(text);
            return outputFormat.format(date);
        } catch (ParseException e) {
            Timber.e(e, "Error parsing data: %s", text);
        }

        return text;
    }
}