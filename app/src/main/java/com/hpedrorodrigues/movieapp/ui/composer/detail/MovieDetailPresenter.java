package com.hpedrorodrigues.movieapp.ui.composer.detail;

public class MovieDetailPresenter implements MovieDetailContract.Presenter {

    private final MovieDetailContract.View view;

    public MovieDetailPresenter(final MovieDetailContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
    }

    @Override
    public void stop() {
    }
}