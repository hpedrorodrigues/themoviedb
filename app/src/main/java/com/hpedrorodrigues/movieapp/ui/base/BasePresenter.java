package com.hpedrorodrigues.movieapp.ui.base;

public interface BasePresenter {

    void start();

    void stop();
}