package com.hpedrorodrigues.movieapp.ui.base.movie;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;

import com.hpedrorodrigues.movieapp.R;
import com.hpedrorodrigues.movieapp.data.constant.Args;
import com.hpedrorodrigues.movieapp.data.remote.ApiResult;
import com.hpedrorodrigues.movieapp.ui.base.BaseActivity;
import com.hpedrorodrigues.movieapp.ui.base.BasePresenter;
import com.hpedrorodrigues.movieapp.ui.composer.detail.MovieDetailActivity;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import static android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import static com.hpedrorodrigues.movieapp.ui.base.movie.BaseMovieAdapter.OnMovieClickListener;

public abstract class BaseMovieListActivity extends BaseActivity {

    protected SuperRecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getPresenter().start();
    }

    @Override
    protected void onView() {
        recyclerView = (SuperRecyclerView) findViewById(R.id.superRecyclerView);
    }

    @Override
    protected void onConfigView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(getAdapter());
        recyclerView.setRefreshListener(getRefreshListener());
        recyclerView.setupMoreListener(getMoreListener(), 5/* Items left to load more */);

        recyclerView.setRefreshingColorResources(
                R.color.colorAccent,
                R.color.colorPrimary,
                R.color.colorPrimaryLight,
                R.color.colorPrimaryDark
        );

        getAdapter().setClickListener(new OnMovieClickListener() {

            @Override
            public void onClick(final ApiResult result) {
                final Bundle args = new Bundle();
                args.putSerializable(Args.MOVIE, result);

                final Intent intent = new Intent(
                        BaseMovieListActivity.this, MovieDetailActivity.class);
                intent.putExtras(args);

                startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        getPresenter().stop();
    }

    public void showError(int resourceId) {
        Snackbar.make(recyclerView, resourceId, Snackbar.LENGTH_SHORT).show();
    }

    protected abstract int getLayoutId();

    protected abstract OnRefreshListener getRefreshListener();

    protected abstract OnMoreListener getMoreListener();

    protected abstract <T extends BaseMovieAdapter<?>> T getAdapter();

    protected abstract <T extends BasePresenter> T getPresenter();
}