package com.hpedrorodrigues.movieapp.ui.composer.search;

import com.hpedrorodrigues.movieapp.ui.common.contract.MovieContract;

public interface MovieSearchContract {

    interface View extends MovieContract.View {
    }

    interface Presenter extends MovieContract.Presenter {

        void loadData(final String query, final boolean appendContent);
    }
}