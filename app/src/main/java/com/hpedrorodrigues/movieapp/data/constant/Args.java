package com.hpedrorodrigues.movieapp.data.constant;

public interface Args {

    String MOVIE = "movie";
    String QUERY = "query";
}