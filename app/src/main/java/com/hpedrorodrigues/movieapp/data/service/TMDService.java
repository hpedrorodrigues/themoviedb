package com.hpedrorodrigues.movieapp.data.service;

import com.hpedrorodrigues.movieapp.data.remote.ApiPage;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface TMDService {

    @GET("/3/discover/movie?sort_by=popularity.desc")
    Observable<ApiPage> getPopularMovies(@Query("page") final int page);

    @GET("/3/search/movie?sort_by=popularity.desc")
    Observable<ApiPage> searchPopularMovies(@Query("page") final int page,
                                            @Query("query") final String query);
}