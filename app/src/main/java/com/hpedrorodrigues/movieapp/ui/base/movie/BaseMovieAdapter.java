package com.hpedrorodrigues.movieapp.ui.base.movie;

import android.view.View;

import com.hpedrorodrigues.movieapp.data.remote.ApiResult;
import com.hpedrorodrigues.movieapp.ui.base.BaseAdapter;
import com.hpedrorodrigues.movieapp.ui.base.BaseHolder;

public abstract class BaseMovieAdapter<V extends BaseHolder> extends BaseAdapter<ApiResult, V> {

    protected OnMovieClickListener clickListener;

    public void setClickListener(OnMovieClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface OnMovieClickListener {

        void onClick(final ApiResult result);
    }

    protected View.OnClickListener getClickListener(final ApiResult result) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (clickListener != null) {
                    clickListener.onClick(result);
                }
            }
        };
    }
}