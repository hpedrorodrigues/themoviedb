package com.hpedrorodrigues.movieapp.ui.composer.list;

import com.hpedrorodrigues.movieapp.ui.common.contract.MovieContract;

public interface MovieListContract {

    interface View extends MovieContract.View {
    }

    interface Presenter extends MovieContract.Presenter {

        void loadData(final boolean appendContent);
    }
}