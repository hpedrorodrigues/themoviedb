package com.hpedrorodrigues.movieapp;

import com.hpedrorodrigues.movieapp.data.format.TMDDataFormatter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TMDDataFormatterTest {

    private TMDDataFormatter formatter;

    @Before
    public void setUp() {
        this.formatter = new TMDDataFormatter();
    }

    @Test
    public void formatAValidDate() throws Exception {
        Assert.assertEquals("2016", formatter.formatDate("2016-03-02"));
    }

    @Test
    public void returnTheSameValueForAInvalidDate() throws Exception {
        Assert.assertEquals("abcd", formatter.formatDate("abcd"));
    }
}