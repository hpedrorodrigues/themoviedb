package com.hpedrorodrigues.movieapp.ui.composer.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.hpedrorodrigues.movieapp.R;
import com.hpedrorodrigues.movieapp.data.constant.Args;
import com.hpedrorodrigues.movieapp.data.remote.ApiPage;
import com.hpedrorodrigues.movieapp.ui.base.movie.BaseMovieListActivity;
import com.hpedrorodrigues.movieapp.ui.composer.search.adapter.SearchMovieAdapter;
import com.malinskiy.superrecyclerview.OnMoreListener;

import javax.inject.Inject;

import static android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;

public class MovieSearchActivity extends BaseMovieListActivity implements MovieSearchContract.View {

    @Inject
    protected SearchMovieAdapter adapter;

    private MovieSearchPresenter presenter;

    private String query;

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        if (this.presenter == null) {
            this.presenter = new MovieSearchPresenter(this);
            getComponent().inject(presenter);
        }

        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            query = savedInstanceState.getString(Args.QUERY);

            presenter.loadData(query, false);
        }
    }

    @Override
    protected void onConfigureActionBar(final ActionBar actionBar) {
        super.onConfigureActionBar(actionBar);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(Args.QUERY, query);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setIconifiedByDefault(false);
        searchView.requestFocus();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String text) {
                if (text == null || text.isEmpty()) {

                    presenter.resetPage();
                    adapter.clear();
                } else {
                    query = text;

                    presenter.resetPage();
                    presenter.loadData(query, false);
                }

                return true;
            }
        });

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void refresh(final ApiPage page) {
        adapter.setContent(page.getResults());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void add(final ApiPage page) {
        adapter.addContent(page.getResults());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.movie_search;
    }

    @Override
    protected void injectMembers() {
        getComponent().inject(this);
    }

    @Override
    protected OnRefreshListener getRefreshListener() {
        return new OnRefreshListener() {

            @Override
            public void onRefresh() {
                presenter.resetPage();
                presenter.loadData(query, false);
            }
        };
    }

    @Override
    protected OnMoreListener getMoreListener() {
        return new OnMoreListener() {

            @Override
            public void onMoreAsked(final int overallItemsCount, final int itemsBeforeMore,
                                    final int maxLastVisiblePosition) {
                presenter.increasePage();
                presenter.loadData(query, true);
            }
        };
    }

    @Override
    protected SearchMovieAdapter getAdapter() {
        return adapter;
    }

    @Override
    protected MovieSearchPresenter getPresenter() {
        return presenter;
    }
}