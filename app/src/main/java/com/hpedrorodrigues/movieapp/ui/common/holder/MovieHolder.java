package com.hpedrorodrigues.movieapp.ui.common.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hpedrorodrigues.movieapp.R;
import com.hpedrorodrigues.movieapp.ui.base.BaseHolder;

public class MovieHolder extends BaseHolder {

    public TextView title;
    public TextView releaseDate;
    public TextView overview;
    public ImageView image;

    public MovieHolder(View view) {
        super(view);
    }

    @Override
    protected void onView(View view) {
        title = (TextView) view.findViewById(R.id.title);
        releaseDate = (TextView) view.findViewById(R.id.release_date);
        overview = (TextView) view.findViewById(R.id.overview);
        image = (ImageView) view.findViewById(R.id.image);
    }
}