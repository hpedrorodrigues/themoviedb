package com.hpedrorodrigues.movieapp.ui.composer.search.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.hpedrorodrigues.movieapp.R;
import com.hpedrorodrigues.movieapp.data.format.TMDDataFormatter;
import com.hpedrorodrigues.movieapp.data.remote.ApiResult;
import com.hpedrorodrigues.movieapp.ui.base.movie.BaseMovieAdapter;
import com.hpedrorodrigues.movieapp.ui.common.holder.MovieHolder;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

public class SearchMovieAdapter extends BaseMovieAdapter<MovieHolder> {

    @Inject
    protected LayoutInflater inflater;

    @Inject
    protected Context context;

    @Inject
    protected TMDDataFormatter formatter;

    @Inject
    public SearchMovieAdapter() {
    }

    @Override
    public MovieHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MovieHolder(inflater.inflate(R.layout.search_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MovieHolder holder, int position) {
        final ApiResult result = getContent().get(position);

        holder.title.setText(result.getTitle());
        holder.overview.setText(result.getOverview());

        final String date = formatter.formatDate(result.getReleaseDate());
        holder.releaseDate.setText(date);

        final String imagePath = formatter.getThumbnailImage(result.getPosterPath());
        Picasso.with(context).load(imagePath).placeholder(R.mipmap.movie).into(holder.image);

        holder.itemView.setOnClickListener(getClickListener(result));
    }
}