package com.hpedrorodrigues.movieapp.injection.module;

import android.content.Context;
import android.view.LayoutInflater;

import com.hpedrorodrigues.movieapp.data.contract.ApiContract;
import com.hpedrorodrigues.movieapp.data.service.TMDService;
import com.hpedrorodrigues.movieapp.data.service.interceptor.AuthorizationInterceptor;
import com.hpedrorodrigues.movieapp.injection.application.AndroidApplication;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AndroidModule {

    private final AndroidApplication application;

    public AndroidModule(final AndroidApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Context provideApplicationContext() {
        return this.application;
    }

    @Provides
    @Singleton
    public OkHttpClient provideClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new AuthorizationInterceptor())
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .build();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(final OkHttpClient client) {
        return new Retrofit
                .Builder()
                .baseUrl(ApiContract.ENDPOINT)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    @Provides
    @Singleton
    public TMDService provideTMDService(final Retrofit retrofit) {
        return retrofit.create(TMDService.class);
    }

    @Provides
    @Singleton
    public LayoutInflater provideLayoutInflater(final Context context) {
        return LayoutInflater.from(context);
    }
}