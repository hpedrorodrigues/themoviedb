package com.hpedrorodrigues.movieapp.data.contract;

import com.hpedrorodrigues.movieapp.BuildConfig;

public interface ApiContract {

    String ENDPOINT = "https://api.themoviedb.org";
    String API_KEY = BuildConfig.TMD_API_KEY;
}