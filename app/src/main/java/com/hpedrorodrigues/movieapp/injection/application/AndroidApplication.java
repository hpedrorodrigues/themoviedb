package com.hpedrorodrigues.movieapp.injection.application;

import android.app.Application;

import com.hpedrorodrigues.movieapp.injection.component.AndroidComponent;
import com.hpedrorodrigues.movieapp.injection.component.DaggerAndroidComponent;
import com.hpedrorodrigues.movieapp.injection.module.AndroidModule;

import timber.log.Timber;

public class AndroidApplication extends Application {

    private AndroidComponent component;

    public AndroidComponent getComponent() {
        return this.component;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Timber
        Timber.plant(new Timber.DebugTree());

        // Dagger
        this.component = DaggerAndroidComponent
                .builder()
                .androidModule(new AndroidModule(this))
                .build();
    }
}