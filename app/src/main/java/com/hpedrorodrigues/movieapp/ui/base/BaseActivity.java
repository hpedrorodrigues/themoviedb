package com.hpedrorodrigues.movieapp.ui.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.hpedrorodrigues.movieapp.injection.application.AndroidApplication;
import com.hpedrorodrigues.movieapp.injection.component.AndroidComponent;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectMembers();
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);

        onView();
        onConfigView();
    }

    @Override
    public void setSupportActionBar(@Nullable Toolbar toolbar) {
        super.setSupportActionBar(toolbar);

        onConfigureActionBar(getSupportActionBar());
    }

    protected abstract void onView();

    protected void onConfigView() {
    }

    protected void injectMembers() {
    }

    protected void onConfigureActionBar(final ActionBar actionBar) {
    }

    protected AndroidApplication getAndroidApplication() {
        return (AndroidApplication) getApplication();
    }

    protected AndroidComponent getComponent() {
        return getAndroidApplication().getComponent();
    }
}
