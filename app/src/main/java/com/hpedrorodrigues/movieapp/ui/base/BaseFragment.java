package com.hpedrorodrigues.movieapp.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hpedrorodrigues.movieapp.injection.component.AndroidComponent;

public abstract class BaseFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        final View view = super.onCreateView(inflater, container, savedInstanceState);

        onView(view);
        onConfigView(view);

        return view;
    }

    protected abstract void onView(final View view);

    protected abstract void onConfigView(final View view);

    protected AndroidComponent getComponent() {
        return ((BaseActivity) getActivity()).getComponent();
    }
}